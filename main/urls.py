from django.contrib import admin
from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.homepage, name='homepage'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('biodata/', views.biodata, name='biodata'),
    path('skills/', views.skills, name='skills'),
    path('hobby/', views.hobby, name='hobby'),
    path('social/', views.social, name='social'),
    path('addmatkul/', views.addmatkul, name='addmatkul'),
    path('viewmatkul/', views.viewmatkul, name='viewmatkul'),
    path('success/', views.success, name='success'),
    path('delete/<int:id>', views.deletedata, name='deletedata')
]
