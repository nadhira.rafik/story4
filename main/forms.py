from django import forms
from .models import data

class dataForm(forms.ModelForm):
    class Meta:
        model = data
        fields = ('namamatkul', 'pengajar', 'periode', 'sks', 'ruang', 'deskripsi')

        widgets = {
            'namamatkul' : forms.TextInput(attrs={'class': 'form-control'}),
            'pengajar' : forms.TextInput(attrs={'class': 'form-control'}),
            'periode' : forms.TextInput(attrs={'class': 'form-control'}),
            'sks' : forms.TextInput(attrs={'class': 'form-control'}),
            'ruang' : forms.TextInput(attrs={'class': 'form-control'}),
            'deskripsi' : forms.TextInput(attrs={'class': 'form-control'}),
        }