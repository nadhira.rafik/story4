from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .models import data
from .forms import dataForm

def homepage(request):
    return render(request, 'homepage.html')
def aboutme(request):
    return render(request, 'aboutme.html')
def biodata(request):
    return render(request, 'biodata.html')
def skills(request):
    return render(request, 'skills.html')
def hobby(request):
    return render(request, 'hobby.html')
def social(request):
    return render(request, 'social.html')
def addmatkul(request):
    if request.method == 'POST':
        form = dataForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'success.html')
    else:
        form = dataForm()
        return render(request, 'addmatkul.html', {'form': form})
def viewmatkul(request):
    viewmatkul = data.objects.all()
    return render(request, 'viewmatkul.html', {'viewmatkul': viewmatkul})
def success(request):
    return render(request, 'sucess.html') 
def deletedata(request, id):
    name = get_object_or_404(data, id=id)
    name.delete()
    return redirect('main:viewmatkul')